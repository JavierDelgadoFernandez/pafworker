#include "PAFProgressUpdatedREST.hpp"

#include <curl/curl.h>


#include "REST.hpp"

#include <iostream>
#include <sstream>
#include <fstream>

PAFProgressUpdatedREST::PAFProgressUpdatedREST(int executionID, const char* host)
	: PAFIProgressUpdated(), fExecutionID(executionID), fHost(host)
{
}

void PAFProgressUpdatedREST::ProgressUpdated(Long64_t total, Long64_t processed)
{
	TString url = TString::Format("http://%s/api/execution/partial/status/%d/", fHost, fExecutionID);
	TString data = TString::Format("{\"id\":\"%d\",\"work-done\":%f,\"status\":\"processing\"}", fExecutionID, ((double)(processed*100))/total);
	POST(url.Data(), data.Data());
}
