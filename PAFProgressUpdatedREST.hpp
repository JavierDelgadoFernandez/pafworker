#pragma once

#include "PAFIProgressUpdated.h"

#include "TList.h"

class PAFProgressUpdatedREST : public PAFIProgressUpdated
{
	public:
		PAFProgressUpdatedREST(int executionID, const char* host);

		virtual void ProgressUpdated(Long64_t total, Long64_t processed);

	protected:
		void SendPOSTData(const char* url, const char* data);

	protected:
		int fExecutionID;
		const char* fHost;
};
