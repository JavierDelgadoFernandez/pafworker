#include "PAFWorker.hpp"

#include "TSystem.h"
#include "TBase64.h"

#include "TString.h"
#include "TCanvas.h"
#include "TImage.h"
#include "TH1.h"
#include "TClass.h"


#include "PAFSequentialEnvironment.h"
#include "PAFPROOFLiteEnvironment.h"
#include "PAFManualSettings.h"
#include "PAFProgressUpdatedREST.hpp"

#include <fstream>

#include "REST.hpp"

const char* PAFWorker::PAFREPOSITORIES_ENV = "PAFREPOSITORIES";
const char* PAFWorker::FIELDS_SEPARATOR = ":";

int main(int argc, const char* argv[])
{
	PAFWorker* runner = new PAFWorker(argv[1], argv[2]);
	runner->Run();
}

PAFWorker::PAFWorker(const char* host, const char* xmlFileName)
	: fXmlFileName(xmlFileName), fExecutionID(0), fHost(host)
{
	TString repositoryPaths(gSystem->Getenv(PAFREPOSITORIES_ENV));
	int firstRepositoryLength = repositoryPaths.First(FIELDS_SEPARATOR) == -1 ? repositoryPaths.Length() : repositoryPaths.First(FIELDS_SEPARATOR);
	fRepositoryPath = TString(gSystem->Getenv(PAFREPOSITORIES_ENV), firstRepositoryLength);
}


void PAFWorker::Run()
{
	PAFProject* project = CreatePAFProject();

	PAFProgressUpdatedREST notifier(fExecutionID, fHost);
	project->SetProgressUpdated(&notifier);

	TList* result = project->Run();
	ProcessResult(result);
	SendROOTFile("pafoutput.root");

	delete project;
}

PAFProject* PAFWorker::CreatePAFProject()
{
	PAFProject* result = new PAFProject();

	TXMLEngine eng;
	XMLDocPointer_t doc = eng.ParseFile(fXmlFileName);
	XMLNodePointer_t root = eng.DocGetRootElement(doc);
	fExecutionID = atoi(eng.GetAttr(root, "id"));

	XMLNodePointer_t environment = eng.GetChild(root);
	TString environmentType = eng.GetAttr(environment, "type");

	if (environmentType.EqualTo("sequential"))
	{
		result->SetExecutionEnvironment(new PAFSequentialEnvironment());
	}
	else if (environmentType.EqualTo("prooflite"))
	{
		Int_t slots = atoi(eng.GetAttr(environment, "slots"));
		result->SetExecutionEnvironment(new PAFPROOFLiteEnvironment(slots));
	}

	XMLNodePointer_t datafiles = eng.GetNext(environment);
	XMLNodePointer_t datafile = eng.GetChild(datafiles);
	while(datafile)
	{
		TString path = TBase64::Decode(eng.GetAttr(datafile, "path"));
		TString tree = TBase64::Decode(eng.GetAttr(datafile, "tree"));

		result->AddDataFile(path.Data(), tree.Data());
		datafile = eng.GetNext(datafile);
	}

	XMLNodePointer_t selectors = eng.GetNext(datafiles);
	XMLNodePointer_t selector = eng.GetChild(selectors);
	while(selector)
	{
		TString name = TBase64::Decode(eng.GetAttr(selector, "name"));

		gSystem->mkdir(TString::Format("%s/%s", fRepositoryPath.Data(), name.Data()).Data(), kTRUE);

		TString headerFileName = TString::Format("%s/%s/%s.h", fRepositoryPath.Data(), name.Data(), name.Data());
		XMLNodePointer_t header = eng.GetChild(selector);
		SaveAsFile(TBase64::Decode(eng.GetNodeContent(header)).Data(), headerFileName.Data());

		TString sourceFileName = TString::Format("%s/%s/%s.C", fRepositoryPath.Data(), name.Data(), name.Data());
		XMLNodePointer_t source = eng.GetNext(header);
		SaveAsFile(TBase64::Decode(eng.GetNodeContent(source)).Data(), sourceFileName.Data());

		result->AddSelectorPackage(name.Data());
		selector = eng.GetNext(selector);
	}
	
	XMLNodePointer_t codes = eng.GetNext(selectors);
	XMLNodePointer_t code = eng.GetChild(codes);
	while(code)
	{
		TString name = TBase64::Decode(eng.GetAttr(code, "name"));

		gSystem->mkdir(TString::Format("%s/%s", fRepositoryPath.Data(), name.Data()).Data(), kTRUE);

		TString headerFileName = TString::Format("%s/%s/%s.h", fRepositoryPath.Data(), name.Data(), name.Data());
		XMLNodePointer_t header = eng.GetChild(code);
		SaveAsFile(TBase64::Decode(eng.GetNodeContent(header)).Data(), headerFileName.Data());

		TString sourceFileName = TString::Format("%s/%s/%s.C", fRepositoryPath.Data(), name.Data(), name.Data());
		XMLNodePointer_t source = eng.GetNext(header);
		SaveAsFile(TBase64::Decode(eng.GetNodeContent(source)).Data(), sourceFileName.Data());

		result->AddPackage (name.Data());
		code = eng.GetNext(code);
	}

	XMLNodePointer_t parameters = eng.GetNext(codes);
	XMLNodePointer_t parameter = eng.GetChild(parameters);
	while(parameter)
	{
		TString name = TBase64::Decode(eng.GetAttr(parameter, "name"));
		TString value = TBase64::Decode(eng.GetAttr(parameter, "value"));
		TString type = TBase64::Decode(eng.GetAttr(parameter, "type"));

		if (type.EqualTo("int"))
		{
			result->SetInputParam(name.Data(), value.Atoi());
		}
		else if (type.EqualTo("float"))
		{
			result->SetInputParam(name.Data(), value.Atof());
		}
		else if (type.EqualTo("tstring"))
		{
			result->SetInputParam(name.Data(), value.Data());
		}
		parameter = eng.GetNext(parameter);
	}
	return result;
}

void PAFWorker::ProcessResult(TList* result)
{
	for(int i = 0; i < result->GetEntries(); i++)
	{
		TObject* item = result->At(i);
		if(item->IsA()->InheritsFrom(TH1::Class()))
		{
			TH1* th1 = (TH1*)item;
			TCanvas c;
			th1->Draw();
			TString name = TString::Format("%d_%s.svg", fExecutionID, item->GetName());
			TString filePath = TString::Format("/tmp/PAFWorker/%s", name.Data());
			c.Print(filePath);
			std::string line,text;
			std::ifstream in(filePath);
			while(std::getline(in, line))
			{
				text += line;
			}
			TString svg_b64 = TBase64::Encode(text.c_str());
			TString url = TString::Format("http://%s/api/execution/partial/result/%d/", fHost, fExecutionID);
			TString data = TString::Format("{\"type\":\"svg\",\"name\":\"%s\",\"svg-source\":\"%s\"}", item->GetName(), svg_b64.Data());
			POST(url.Data(), data.Data());
		}
	}
}

void PAFWorker::SaveAsFile(const char* data, const char* path)
{
	std::fstream file(path, std::fstream::out);
	file << data;
	file.flush();
	file.close();
}

void PAFWorker::SendROOTFile(const char* path)
{
	char * buffer;
	long size;
	std::ifstream file (path, std::ios::in|std::ios::binary|std::ios::ate);
	size = file.tellg();
	file.seekg (0, std::ios::beg);
	buffer = new char [size];
	file.read (buffer, size);
	file.close();

	TString file_b64 = TBase64::Encode(buffer, size);

	TString url = TString::Format("http://%s/api/execution/partial/result/%d/", fHost, fExecutionID);
	TString data = TString::Format("{\"type\":\"root\", \"root-source\":\"%s\"}", file_b64.Data());
	POST(url.Data(), data.Data());
}
