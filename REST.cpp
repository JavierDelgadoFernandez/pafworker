#include "REST.hpp"

#include <curl/curl.h>

void POST(const char* url, const char* data)
{
	CURL* curl = 0;
	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();

  	if(curl) {
		struct curl_slist* chunk = NULL;

		curl_easy_setopt(curl, CURLOPT_URL, url);

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);

		chunk = curl_slist_append(chunk, "Content-Type: application/json");

		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

		curl_easy_perform(curl);

		curl_easy_cleanup(curl);
	}
}
