
#include "PAFProject.h"

#include "TXMLEngine.h"

class PAFWorker
{
	public:
		PAFWorker(const char* host, const char* xmlFileName);

		virtual void Run();

	protected:
		virtual PAFProject* CreatePAFProject();
		virtual void ProcessResult(TList* result);
		virtual void SendROOTFile(const char* path);
		virtual void SaveAsFile(const char* data, const char* path);

	protected:
		TString fRepositoryPath;
		const char* fXmlFileName;
		int fExecutionID;
		const char* fHost;

		static const char* PAFREPOSITORIES_ENV;
		static const char* FIELDS_SEPARATOR;
};
